package sync

import (
	"context"
	"database/sql"
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"strconv"

	"cloud.google.com/go/pubsub"

	kitlog "github.com/go-kit/kit/log"

	_ "github.com/lib/pq"
)

type PubSubMessage struct {
	Data []byte `json:"data"`
}

var (
	psql   *sql.DB
	logger kitlog.Logger

	topic *pubsub.Topic
)

func init() {
	var err error

	{
		logger = kitlog.NewJSONLogger(os.Stdout)
		logger = kitlog.With(logger, "timestamp", kitlog.DefaultTimestampUTC, "transport", "pubsub")
	}

	{
		psql, err = sql.Open("postgres", os.Getenv("DATABASE_URL"))
		if err != nil {
			logger.Log("level", "fatal", "msg", "could not connect to database", "err", err)
			os.Exit(1)
		}
		logger.Log("level", "debug", "msg", "connected to database", "url", os.Getenv("DATABASE_URL"))
	}

	// Only allow 1 connection to the database to avoid overloading it.
	psql.SetMaxIdleConns(1)
	psql.SetMaxOpenConns(1)

	{
		client, err := pubsub.NewClient(context.Background(), "synkt-1556838469966")
		if err != nil {
			logger.Log("level", "fatal", "msg", "error creating pubsub client", "err", err)
			os.Exit(1)
		}

		topic = client.Topic("sync-set-prices")
	}
}

type scryfallSearch struct {
	HasMore  bool           `json:"has_more"`
	NextPage string         `json:"next_page"`
	Data     []scryfallCard `json:"data"`
}

type scryfallCard struct {
	ID     string `json:"id"`
	Prices struct {
		NearMint string `json:"usd"`
		Foil     string `json:"usd_foil"`
	}
}

func SyncSet(ctx context.Context, m PubSubMessage) error {
	var (
		url string

		resp *http.Response
		err  error
	)
	logger.Log("level", "debug", "msg", "received message", "data", string(m.Data))

	url = fmt.Sprintf("https://api.scryfall.com/cards/search?format=json&include_extras=false&include_multilingual=false&order=set&page=1&q=e%%3A%s&unique=prints", string(m.Data))

	for {
		resp, err = http.Get(url)
		if err != nil {
			logger.Log("level", "error", "msg", "error executing http request", "err", err)
			return err
		}
		defer resp.Body.Close()

		var result scryfallSearch

		err = json.NewDecoder(resp.Body).Decode(&result)
		if err != nil {
			logger.Log("level", "error", "msg", "error decoding json", "err", err)
			return err
		}

		if len(result.Data) == 0 {
			return fmt.Errorf("No results found in scryfall for %s", m.Data)
		}

		for _, c := range result.Data {
			if c.Prices.NearMint != "" {
				var price float64
				price, err = strconv.ParseFloat(c.Prices.NearMint, 64)
				if err != nil {
					return err
				}

				_, err = psql.Exec(`INSERT INTO card_condition_price_histories (
					card_condition_id, date, price, created_at, updated_at
				) SELECT
					(SELECT card_conditions.id FROM card_conditions JOIN cards ON cards.id = card_id WHERE scryfall_id = $1 AND condition = 'near-mint' LIMIT 1),
					CURRENT_DATE,
					$2,
					now(),
					now()
				WHERE EXISTS (
					SELECT card_conditions.id FROM card_conditions JOIN cards ON cards.id = card_id WHERE scryfall_id = $1 AND condition = 'near-mint' LIMIT 1
				)`, c.ID, int(price*100))
			}

			if c.Prices.Foil != "" {
				var price float64
				price, err = strconv.ParseFloat(c.Prices.Foil, 64)
				if err != nil {
					return err
				}

				_, err = psql.Exec(`INSERT INTO card_condition_price_histories (
					card_condition_id, date, price, created_at, updated_at
				) SELECT
					(SELECT card_conditions.id FROM card_conditions JOIN cards ON cards.id = card_id WHERE scryfall_id = $1 AND condition = 'foil-near-mint' LIMIT 1),
					CURRENT_DATE,
					$2,
					now(),
					now()
				WHERE EXISTS (
					SELECT card_conditions.id FROM card_conditions JOIN cards ON cards.id = card_id WHERE scryfall_id = $1 AND condition = 'foil-near-mint' LIMIT 1
				)`, c.ID, int(price*100))
			}
		}

		if !result.HasMore {
			break
		}

		url = result.NextPage
	}

	return err
}
